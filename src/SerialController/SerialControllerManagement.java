/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SerialController;

import UIController.BatteryCheckMainUI;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import gnu.io.CommPortIdentifier; 
import gnu.io.SerialPort;
import gnu.io.SerialPortEvent; 
import gnu.io.SerialPortEventListener; 
import java.io.IOException;
import java.util.Enumeration;
import java.util.Scanner;

/**
 *
 * @author mobilehunter
 */
public class SerialControllerManagement implements SerialPortEventListener {
    private BatteryCheckMainUI mainUI = null;
    
    SerialPort serialPort;

    /** The port we're normally going to use. */
    private static final String PORT_NAMES[] = { 
        //"/dev/ttyACM0", //for Ubuntu
        //"/dev/tty.usbserial-A9007UX1", // Mac OS X
        //"/dev/tty.usbserial-AI0523F2",
        //"/dev/ttyUSB0", // Linux
        //"COM3", // Windows
        //"/dev/cu.usbserial-AI0523F2",
        "/dev/tty.usbserial-AI033NVG",
     };
    
    /**
    * A BufferedReader which will be fed by a InputStreamReader 
    * converting the bytes into characters 
    * making the displayed results codepage independent
    */
    private BufferedReader input;
    /** The output stream to the port */
    private OutputStream output;
    /** Milliseconds to block while waiting for port open */
    private static final int TIME_OUT = 2000;
    /** Default bits per second for COM port. */
    private static final int DATA_RATE = 57600;

    private void displayLog(String strLog) {
        if (mainUI != null) {
            mainUI.displayLog(strLog);
        }
    }
    private void displayLogln(String strLog) {
        if (mainUI != null) {
            mainUI.displayLogln(strLog);
        }
    }
    private void displayLink() {
        if (mainUI != null) {
            mainUI.displayLink();
        }
    }
    private void setSerialPortName(String strSerialPortName) {
        if (mainUI != null) {
            mainUI.setSerialPortName(strSerialPortName);
        }
    }
    public void initialize(BatteryCheckMainUI pMainUI) {
        this.mainUI = pMainUI;
        
        CommPortIdentifier portId = null;
        Enumeration portList = CommPortIdentifier.getPortIdentifiers();

        //First, Find an instance of serial port as set in PORT_NAMES.
        while (portList.hasMoreElements()) {
            CommPortIdentifier currPortId = (CommPortIdentifier) portList.nextElement();
            for (String portName : PORT_NAMES) {
                if (currPortId.getName().equals(portName)) {
                    if (currPortId.getPortType() == CommPortIdentifier.PORT_SERIAL) {
                        portId = currPortId;
                        setSerialPortName(portName);
                        break;
                    }
                }
            }
        }
        
        if (portId == null) {
            displayLogln("Could not find COM port.");
            return;
        }

        try {
            // open serial port, and use class name for the appName.
            String Name = this.getClass().getName();
            serialPort = (SerialPort) portId.open(Name, TIME_OUT);

            // set port parameters
            serialPort.setSerialPortParams(DATA_RATE,
                SerialPort.DATABITS_8,
                SerialPort.STOPBITS_1,
                SerialPort.PARITY_NONE);

            // open the streams
            input = new BufferedReader(new InputStreamReader(serialPort.getInputStream()));
            output = serialPort.getOutputStream();
            
            // add event listeners
            serialPort.addEventListener(this);
            serialPort.notifyOnDataAvailable(true);
        } catch (Exception e) {
            displayLogln("Exception : " + e.toString());
            mainUI.setSerialStatus(false);
        }
    }

    /**
     * This should be called when you stop using the port.
     * This will prevent port locking on platforms like Linux.
     */
    public synchronized void close() {
        if (serialPort != null) {
            serialPort.removeEventListener();
            serialPort.close();
        }
    }
    
    final static char[] STX = {0x02};
    final static char[] ETX = {0x03};
    final static String strSTX = new String(STX);
    final static String strETX = new String(ETX);

    /**
     * Handle an event on the serial port. Read the data and print it.
     */
    public synchronized void serialEvent(SerialPortEvent oEvent) {
        //System.out.println("oEvent: " + oEvent.toString());
        switch (oEvent.getEventType()) {
            case SerialPortEvent.BI:
            case SerialPortEvent.OE:
            case SerialPortEvent.FE:
            case SerialPortEvent.PE:
            case SerialPortEvent.CD:
            case SerialPortEvent.CTS:
            case SerialPortEvent.DSR:
            case SerialPortEvent.RI:
            case SerialPortEvent.OUTPUT_BUFFER_EMPTY:
                break;
            case SerialPortEvent.DATA_AVAILABLE:
                try {
                    String readLine = input.readLine();
                    
                    if ((readLine.startsWith(strETX) || readLine.startsWith(strSTX)) 
                            && readLine.contains("@$ M99")){
                        displayLink();
                    } else {
                        displayLogln("Read Line: " + readLine);
                    }
                } catch (Exception e) {
                    System.err.println(e.toString());
                }
                break;
        }
        // Ignore all the other eventTypes, but you should consider the other ones.
    }

    public void send(String message) throws IOException {
        // sscanf(str, "%5s %4d-%2d-%2d %2d:%2d:%2d",
        //temp, &year, &mon, &day, &hour, &min, &sec) != 7)
        if (!message.startsWith(strSTX)) {
            message = strSTX + message;
        }
        if (!message.endsWith("\r\n" + strETX)) {
            if (message.endsWith("\r\n")) {
                message = message.substring(0, message.length()-2);
            } else if (message.endsWith("\n") || message.endsWith("\r")) {
                message = message.substring(0, message.length()-1);
            } 
            message = message + "\r\n" + strETX;
        }
        //message = 0x02+ "@$ M0,2,2,0,1\r\n"+0x03;
        byte[] bMessage = message.getBytes();
        System.out.print(bMessage[0] + ".");
        System.out.print(bMessage[1] + ".");
        output.write(bMessage);
        displayLogln("Requested: " + message.substring(1, message.length()-3));
    }
}
